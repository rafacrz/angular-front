import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class GuardAuthGuard implements CanActivate {

  constructor(
    private router: Router
  ) {}
    //private authService: AuthService


  canActivate() {
    if (true){
      return true;
    }

    this.router.navigate(['/']);
    return false;
  }
}
