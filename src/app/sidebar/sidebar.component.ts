import { Component, OnInit } from '@angular/core';

declare var $:any;

export interface RouteInfo {
    path: string;
    title: string;
    icon: string;
    class: string;
}

export const ROUTES: RouteInfo[] = [
    { path: 'dashboard', title: 'Dashboard',  icon: 'ti-panel', class: '' },
    { path: 'user', title: 'Contas',  icon:'ti-user', class: '' },
    { path: 'table', title: 'Cartão de crédito',  icon:'ti-view-list-alt', class: '' },
    { path: 'typography', title: 'Contas a pagar',  icon:'ti-text', class: '' },
    { path: 'icons', title: 'Contas a receber',  icon:'ti-pencil-alt2', class: '' }
];

@Component({
    moduleId: module.id,
    selector: 'sidebar-cmp',
    templateUrl: 'sidebar.component.html',
})

export class SidebarComponent implements OnInit {
    public menuItems: any[];
    ngOnInit() {
        this.menuItems = ROUTES.filter(menuItem => menuItem);
    }
    isNotMobileMenu(){
        if($(window).width() > 991){
            return false;
        }
        return true;
    }

}
